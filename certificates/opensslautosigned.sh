

openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout amazon.artifakt.test.key \
    -new \
    -out artifakt.test.crt \
    -subj /CN=\*.artifakt.test \
	-sha256 \
    -days 3650
	
#    -config <(cat /System/Library/OpenSSL/openssl.cnf \
##        <(printf '[SAN]\nsubjectAltName=DNS:\*.artifakt.test')) \

#    -reqexts SAN \
#    -extensions SAN \
#openssl req -x509 -days 365 -nodes -newkey rsa:1024 \
#              -keyout key.pem -out cert.pem