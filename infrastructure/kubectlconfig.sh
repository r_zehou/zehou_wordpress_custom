#!/usr/bin/env bash

REGION_CODE=eu-west-2
CLUSTER_NAME=eks_cluster_artifakt_test


aws eks --region $REGION_CODE update-kubeconfig --name $CLUSTER_NAME 
##--role-arn $EKS_SERVICE_ROLE
## aws eks --region eu-west-2 update-kubeconfig --name eks_cluster_artifakt_test 


kubectl get svc ## We need to be sure that we are able to connect to the kubernetes-cluster created
