


#!/usr/bin/env bash


EKS_VPC=cloud2artifakttest
EKS_SERVICE_ROLE=eksServiceRole
REGION_CODE=eu-west-2
CLUSTER_NAME=eks_cluster_artifakt_test
##ROLE_NODE_worker=arn:aws:iam::844072746974:role/eksworkernoderole-NodeInstanceRole-KT1ZA2NPOFFT
export ROLENODE=$(aws cloudformation describe-stacks --stack-name nodegrouprole | jq -r '.Stacks[0].Outputs[0].OutputValue')
export SUBNETID2=$(aws cloudformation describe-stacks --stack-name cloud2artifakttest | jq -r '.Stacks[0].Outputs[2].OutputValue')


##sed -f replace.sed nodegroup_template.yaml > nodegroup_template_fixed.yaml
cat nodegroup_template.yaml | envsubst > nodegroup_template_fixed.yaml

aws cloudformation deploy --stack-name nodegroupeksdeployment --template-file nodegroup_template_fixed.yaml --parameter-overrides ClusterName=$CLUSTER_NAME,NodeGroupName=worker1,NodeRole=$ROLE_NODE_GROUP,Subnets=$SUBNET_ID_2
