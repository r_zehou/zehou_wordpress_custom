#!/usr/bin/env bash


EKS_VPC=cloud2artifakttest
EKS_SERVICE_ROLE=eksServiceRole
REGION_CODE=eu-west-2
CLUSTER_NAME=eks_cluster_artifakt_test
ROLE_ARN=$(aws cloudformation describe-stacks --stack-name eksServiceRole | jq -r '.Stacks[0].Outputs[0].OutputValue')
ROLE_NODE_GROUP=$(aws cloudformation describe-stacks --stack-name nodegrouprole | jq -r '.Stacks[0].Outputs[0].OutputValue')
SECURITY_GROUP_ID=$(aws cloudformation describe-stacks --stack-name cloud2artifakttest | jq -r '.Stacks[0].Outputs[0].OutputValue')
SUBNET_ID_1=$(aws cloudformation describe-stacks --stack-name cloud2artifakttest | jq -r '.Stacks[0].Outputs[1].OutputValue')
SUBNET_ID_2=$(aws cloudformation describe-stacks --stack-name cloud2artifakttest | jq -r '.Stacks[0].Outputs[2].OutputValue')

aws eks --region $REGION_CODE create-cluster \
   --name $CLUSTER_NAME --kubernetes-version 1.15 \
   --role-arn \
      $ROLE_ARN \
   --resources-vpc-config \
      subnetIds=$SUBNET_ID_2,securityGroupIds=$SECURITY_GROUP_ID
	  
## We must put a WAIT here, because it takes time to build a kubernetes service

