

#!/usr/bin/env bash



aws iam create-policy --policy-name Amazon_EBS_CSI_Driver \--policy-document file://stockage-iam-policy.json

aws iam attach-role-policy \
--policy-arn arn:aws:iam::844072746974:policy/Amazon_EBS_CSI_Driver \
--role-name nodegrouprole-NodeInstanceRole-7UZE8XUWGV7E

kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"


