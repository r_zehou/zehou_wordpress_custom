# zehou_wordpress_custom



Requirements :


- AWS Account
- Aws CLI with administrator credentials or an account able to create role
- Kubectl
- Jq for JSON parsing


Mains Steps :

1) Deploy the infrastructure and EKS service

We will need to deploy a virtual cloud network, somes roles and the EKS service

2) Deploy the nodegroup is the EKS Service and configure kubectl

We need to provisionne one node (minimum) for the EKS Service

3) Provisionning the permanent sharestockage

We need to provisionne a permanent sharestockage for the MySQL database and the wordpress source website

4) Namespace, LoadBalancer, Services and Secrets

We need to create a namespace in Kubernetes, inside we will create all the ressources, we will begin with the secrets, then the loadbalancer and the services

5) Deploy the applicative stacks

Deploy the two pods, wordpress and MySQLdatabase

6) Get the external-IP inside the services

